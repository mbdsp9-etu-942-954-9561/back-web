import {IAnnexe} from "./IAnnexe";

export interface ICitoyen {
  id?: number;
  name: string;
  firstname: string;
  birthday: Date;
  gender: string;
  idcard?: string;
  deathdate?: Date;
  lifeStatus: IAnnexe;
  password?: string;
}
