import {ICitoyen} from "./ICitoyen";

export interface IFamille {
    id?: number;
    familylead: ICitoyen;
    qrcode: string;
  }
