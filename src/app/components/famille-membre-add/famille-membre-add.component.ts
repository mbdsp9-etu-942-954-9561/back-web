import {Component, EventEmitter, Input, Output} from '@angular/core';
import {IMembreFamille} from "../../utils/interface/IMembreFamille";
import {ICitoyen} from "../../utils/interface/ICitoyen";
import {IFamille} from "../../utils/interface/IFamille";
import {FamilleService} from "../../utils/service/famille/famille.service";

@Component({
  selector: 'app-famille-membre-add',
  templateUrl: './famille-membre-add.component.html',
  styleUrls: ['./famille-membre-add.component.scss']
})
export class FamilleMembreAddComponent {

  @Input() family = {
    familylead: {} as ICitoyen
  } as IFamille
  familleMembre = {
    citizen: {
    } as ICitoyen
  } as IMembreFamille;
  error = "";

  @Output() newMembreFamille = new EventEmitter<IMembreFamille>()

  constructor(private familleService: FamilleService) {
  }

  onSubmit() {
    this.familleMembre.family = this.family
    this.familleService.addMembreFamille(this.familleMembre).subscribe({
      next: (response) => {
        this.familleMembre = {
          citizen: {} as ICitoyen
        } as IMembreFamille;
        this.newMembreFamille.emit(response);
      },
      error: (error) => this.error = error.message
    })
  }
}
