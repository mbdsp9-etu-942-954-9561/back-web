import { TestBed } from '@angular/core/testing';

import { AdminLourdService } from './admin-lourd.service';

describe('AdminLourdService', () => {
  let service: AdminLourdService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AdminLourdService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
