import {Component, EventEmitter, Input, Output} from '@angular/core';
import {IInvestissement} from 'src/app/utils/interface/IInvestissement';
import {InvestissementService} from "../../utils/service/investissement/investissement.service";
import {IProjet} from "../../utils/interface/IProjet";

@Component({
  selector: 'app-investissement-add',
  templateUrl: './investissement-add.component.html',
  styleUrls: ['./investissement-add.component.scss']
})
export class InvestissementAddComponent {
  investissement = {} as IInvestissement;
  @Input() projetId = -1;

  @Output() newInvestissement = new EventEmitter<IInvestissement>();

  error = "";

  constructor(
    private investissementService: InvestissementService
  ) {
  }


  onSubmit() {
    this.investissement.projet = {id: this.projetId} as IProjet;
    this.investissementService.create(this.investissement).subscribe({
      next: (response) => {
        this.investissement = {} as IInvestissement;
        this.newInvestissement.emit(response);
      },
      error: (error) => this.error = error.message
    })
  }
}
