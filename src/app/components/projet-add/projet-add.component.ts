import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {IProjet} from "../../utils/interface/IProjet";
import {ProjetService} from "../../utils/service/projet/projet.service";
import {ProjetCategoryService} from "../../utils/service/projet-category/projet-category.service";
import {IProjetCategory} from "../../utils/interface/IAnnexe";

@Component({
  selector: 'app-projet-add',
  templateUrl: './projet-add.component.html',
  styleUrls: ['./projet-add.component.scss']
})
export class ProjetAddComponent implements OnInit {
  formData = {} as IProjet;
  error = "";
  projetCategories: IProjetCategory[] = [];

  @Output() newProjet = new EventEmitter<IProjet>();

  constructor(
    private projetService: ProjetService,
    private projetCategoryService: ProjetCategoryService
  ) {
    this.formData.projetCategory = {} as IProjetCategory
  }

  ngOnInit() {
    this.projetCategoryService.getList().subscribe({
      next: (response) => {
        this.projetCategories = response;
      },
      error: (error) => this.error = error.message
    })
  }

  onSubmit() {
    // Vous pouvez ici traiter les données soumises comme vous le souhaitez, par exemple, les envoyer à un serveur via une API.

    this.projetService.create(this.formData).subscribe({
      next: (response) => {
        this.formData = {} as IProjet;
        this.formData.projetCategory = {} as IProjetCategory;
        this.newProjet.emit(response);
      },
      error: (error) => this.error = error.message
    })
  }
}
