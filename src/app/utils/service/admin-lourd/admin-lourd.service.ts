import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {IAdminLourd} from "../../interface/IAdminLourd";

@Injectable({
  providedIn: 'root'
})
export class AdminLourdService {

  private baseUrl = "api/adminLourd/";
  constructor(private httpClient:HttpClient) {
  }

  login(admin: IAdminLourd): Observable<IAdminLourd> {
    const url = this.baseUrl + "login";
    return this.httpClient
      .post<IAdminLourd>(url, admin);
  }
}
