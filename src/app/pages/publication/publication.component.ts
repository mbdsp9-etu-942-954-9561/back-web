import {Component, OnInit} from '@angular/core';
import { IPublication } from 'src/app/utils/interface/IPublication';
import {PublicationService} from "../../utils/service/publication/publication.service";

@Component({
  selector: 'app-publication',
  templateUrl: './publication.component.html',
  styleUrls: ['./publication.component.scss']
})
export class PublicationComponent implements OnInit {
  publications: IPublication[] = [];
  error = "";

  constructor(private publicationService: PublicationService) {

  }


  ngOnInit(): void {
    this.publicationService.getList().subscribe({
      next: (response) => {
        this.publications = response;
      },
      error: (error) => this.error = error.message
    })
  }

  addPublication(publication:IPublication){
    this.publications.push(publication);
  }

}
