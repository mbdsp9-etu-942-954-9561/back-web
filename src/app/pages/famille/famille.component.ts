import {Component, OnInit} from '@angular/core';
import { IFamille } from 'src/app/utils/interface/IFamille';
import {FamilleService} from "../../utils/service/famille/famille.service";

@Component({
  selector: 'app-famille',
  templateUrl: './famille.component.html',
  styleUrls: ['./famille.component.scss']
})
export class FamilleComponent implements OnInit{
  familles: IFamille[] = [];
  error="";
  link = "/familleDetail/"

  constructor(
    private familleService: FamilleService,
  ) { }

  ngOnInit(): void {
    this.familleService.getList().subscribe({
      next: (response) => {
        this.familles = response;
      },
      error: (error) => this.error = error.message
    })
  }

  addFamille(famille: IFamille){
    this.familles.push(famille);
  }
}
