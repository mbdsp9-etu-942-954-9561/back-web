import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {IProjet} from "../../interface/IProjet";
import {IProjetCategory} from "../../interface/IAnnexe";

@Injectable({
  providedIn: 'root'
})
export class ProjetCategoryService {
  baseUrl = "/api/projetCategory/";

  constructor(private httpClient: HttpClient) {
  }

  getList(): Observable<IProjetCategory[]> {
    const url = this.baseUrl + "read";
    return this.httpClient.get<IProjetCategory[]>(url);
  }
}
