import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {IProjet, IProjetPredition} from "../../interface/IProjet";
import {IInvestissement} from "../../interface/IInvestissement";

@Injectable({
  providedIn: 'root'
})
export class ProjetService {

  baseUrl = "/api/projet/";
  baseSondage = "/api/sondage/";

  constructor(private httpClient: HttpClient) {
    
  }

  getList(): Observable<IProjet[]> {
    const url = this.baseUrl + "read";
    return this.httpClient.get<IProjet[]>(url);
  }

  create(projet: IProjet): Observable<IProjet> {
    const url = this.baseUrl + "create";
    return this.httpClient.post<IProjet>(url, projet);
  }

  getListInvestissement(projetId: number, type: number): Observable<IInvestissement[]> {
    const url = this.baseUrl + "invest/" + projetId + "/" + type;
    return this.httpClient.get<IInvestissement[]>(url);
  }

  prediction(idProjet: number | string): Observable<IProjetPredition> {
    const url = this.baseSondage + "prediction/" + idProjet;
    return this.httpClient
      .get<IProjetPredition>(url);
  }
}
