import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {IDemandeDocument} from "../../interface/IDemandeDocument";

@Injectable({
  providedIn: 'root'
})
export class DemandeDocumentService {
  baseUrl = "api/demandeDocument/";

  constructor(private httpClient: HttpClient) {
  }

  getList(): Observable<IDemandeDocument[]> {
    const url = this.baseUrl + "read";
    return this.httpClient.get<IDemandeDocument[]>(url);
  }
}
