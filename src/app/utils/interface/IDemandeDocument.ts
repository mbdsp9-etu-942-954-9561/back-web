import {ICitoyen} from "./ICitoyen";
import { ITypeDocument } from "./IAnnexe";

export interface IDemandeDocument {
  id?: number;
  citizen: ICitoyen;
  document: ITypeDocument;
  requestDate?: Date;
}
