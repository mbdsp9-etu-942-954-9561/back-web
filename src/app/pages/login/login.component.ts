import {Component} from '@angular/core';
import {Router} from "@angular/router";
import {AdminLourdService} from "../../utils/service/admin-lourd/admin-lourd.service";
import {IAdminLourd} from "../../utils/interface/IAdminLourd";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent {
  user = '';
  pwd = '';
  error = "";

  constructor(
    private router: Router,
    private adminLourdService: AdminLourdService
  ) {
  }

  onSubmit() {

    const admin = {username: this.user, password: this.pwd} as IAdminLourd;

    this.adminLourdService.login(admin).subscribe({
      next: (response) => {
        localStorage.setItem("user", JSON.stringify(response));
        this.router.navigate(["projet"])
      },
      error: (error) => this.error = error.message
    })
  }
}
