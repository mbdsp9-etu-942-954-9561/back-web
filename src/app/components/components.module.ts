import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {CommentSectionComponent} from "./comment-section/comment-section.component";
import {ErrorMessageComponent} from "./error-message/error-message.component";
import {FamilleAddComponent} from "./famille-add/famille-add.component";
import {InvestissementAddComponent} from "./investissement-add/investissement-add.component";
import {LeftbarComponent} from "./leftbar/leftbar.component";
import {ProjetAddComponent} from "./projet-add/projet-add.component";
import {PublicationFormComponent} from "./publication-form/publication-form.component";
import {FormsModule} from "@angular/forms";
import {RouterLink, RouterLinkActive} from "@angular/router";
import { FamilleMembreAddComponent } from './famille-membre-add/famille-membre-add.component';



@NgModule({
  declarations: [
    CommentSectionComponent,
    ErrorMessageComponent,
    FamilleAddComponent,
    InvestissementAddComponent,
    LeftbarComponent,
    ProjetAddComponent,
    PublicationFormComponent,
    FamilleMembreAddComponent
  ],
  exports: [
    ErrorMessageComponent,
    ProjetAddComponent,
    LeftbarComponent,
    FamilleAddComponent,
    InvestissementAddComponent,
    PublicationFormComponent,
    CommentSectionComponent,
    FamilleMembreAddComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    RouterLinkActive,
    RouterLink
  ]
})
export class ComponentsModule { }
