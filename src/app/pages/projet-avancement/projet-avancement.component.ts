import {Component, OnInit} from '@angular/core';
import {IInvestissement} from 'src/app/utils/interface/IInvestissement';
import {ProjetService} from "../../utils/service/projet/projet.service";
import {ActivatedRoute} from "@angular/router";

@Component({
  selector: 'app-projet-avancement',
  templateUrl: './projet-avancement.component.html',
  styleUrls: ['./projet-avancement.component.scss']
})
export class ProjetAvancementComponent implements OnInit {

  error = "";

  investissementEntrees: IInvestissement[] = [];
  investissementSorties: IInvestissement[] = [];

  constructor(
    private projetService: ProjetService,
    private route: ActivatedRoute
  ) {
  }

  projetId = -1;

  ngOnInit(): void {
    this.projetId = this.route.snapshot.params['projetId']!;
    this.projetService.getListInvestissement(this.projetId, 1).subscribe({
      next: (response) => {
        this.investissementEntrees = response;
      },
      error: (error) => this.error = error.message
    })
    this.projetService.getListInvestissement(this.projetId, 2).subscribe({
      next: (response) => {
        this.investissementSorties = response;
      },
      error: (error) => this.error = error.message
    })
  }

  addInvestissement(investissement: IInvestissement) {
    if (investissement.type==1)
      this.investissementEntrees.push(investissement);
    else if (investissement.type==2)
      this.investissementSorties.push(investissement);
  }
}
