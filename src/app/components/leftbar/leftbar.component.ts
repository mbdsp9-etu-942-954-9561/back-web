import {Component} from '@angular/core';
import {Router} from "@angular/router";

@Component({
  selector: 'app-leftbar',
  templateUrl: './leftbar.component.html',
  styleUrls: ['./leftbar.component.scss']
})
export class LeftbarComponent {
  menus: Array<{
    icon: string;
    title: string;
    link: string;
    show: boolean
  }> = [
    {
      icon: '',
      title: 'Projet',
      link: '/projet',
      show: true
    },
    {
      icon: '',
      title: 'Gestion famille',
      link: '/famille',
      show: true
    },
    {
      icon: '',
      title: 'Post',
      link: '/publication',
      show: true
    },
    {
      icon: '',
      title: 'Document',
      link: '/document',
      show: true
    }
  ];

  labelConnexion = "Se connecter";

  constructor(private router: Router) {
  }

  logOut(){
    localStorage.removeItem("user")
    this.router.navigate(["login"])
  }
}
