import {Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges} from '@angular/core';
import {ICommentaire} from 'src/app/utils/interface/ICommentaire';
import {PublicationService} from "../../utils/service/publication/publication.service";

@Component({
  selector: 'app-comment-section',
  templateUrl: './comment-section.component.html',
  styleUrls: ['./comment-section.component.scss']
})
export class CommentSectionComponent implements OnInit {
  @Input() publicationId: number | string = 0;
  comments: ICommentaire[] = []
  error: string = "";


  constructor(
    private publicationService: PublicationService
  ) {

  }

  ngOnInit(): void {
    this.getComments();

  }

  getComments(): void {
    this.publicationService.getComment(this.publicationId).subscribe({
      next: (response) => {
        this.comments = response;
      },
      error: (error) => this.error = error.message
    })
  }

  /*addComment(): void {
    if (this.newComment.trim() !== '') {

      const userJson = localStorage.getItem("user")!;
      const admin = JSON.parse(userJson) as IAdminLourd;
      const comment: ICommentaire= {
        citizen: admin,
        publication : {id: this.publicationId},
        comment: this.newComment,
      };

      //AddComment
    }
  }*/
}
