import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {ProjetService} from "../../utils/service/projet/projet.service";
import {IProjetPredition} from "../../utils/interface/IProjet";

@Component({
  selector: 'app-sondage-form',
  templateUrl: './sondage-form.component.html',
  styleUrls: ['./sondage-form.component.scss']
})
export class SondageFormComponent implements OnInit {

  error = ""
  projetId: number = 0;
  projetPrediction= {} as IProjetPredition

  constructor(
    private route: ActivatedRoute,
    private projetService: ProjetService
  ) {

  }

  ngOnInit(): void {
    this.projetId = this.route.snapshot.params['projetId']!;
    this.projetService.prediction(this.projetId).subscribe({
      next: (response) => {
        this.projetPrediction = response;
      },
      error: (error) => this.error = error.message
    })
  }
}
