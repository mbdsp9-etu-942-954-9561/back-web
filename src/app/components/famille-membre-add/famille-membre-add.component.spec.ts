import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FamilleMembreAddComponent } from './famille-membre-add.component';

describe('FamilleMembreAddComponent', () => {
  let component: FamilleMembreAddComponent;
  let fixture: ComponentFixture<FamilleMembreAddComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [FamilleMembreAddComponent]
    });
    fixture = TestBed.createComponent(FamilleMembreAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
