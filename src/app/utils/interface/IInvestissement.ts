import {IProjet} from "./IProjet";

export interface IInvestissement {
    id?: number;
    projet: IProjet;
    label: string;
    price: number;
    transactionDate: Date;
    type: number;
}
