import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {IProjet} from "../../interface/IProjet";
import {IInvestissement} from "../../interface/IInvestissement";

@Injectable({
  providedIn: 'root'
})
export class InvestissementService {

  baseUrl = "api/investissement/";

  constructor(private httpClient: HttpClient) {
  }

  create(investissement: IInvestissement): Observable<IInvestissement> {
    const url = this.baseUrl + "create";
    return this.httpClient.post<IInvestissement>(url, investissement);
  }
}
