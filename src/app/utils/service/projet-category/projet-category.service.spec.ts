import { TestBed } from '@angular/core/testing';

import { ProjetCategoryService } from './projet-category.service';

describe('ProjetCategoryService', () => {
  let service: ProjetCategoryService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ProjetCategoryService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
