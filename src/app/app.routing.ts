import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './pages/login/login.component';
import { ProjetComponent } from './pages/projet/projet.component';
import { ProjetAvancementComponent } from './pages/projet-avancement/projet-avancement.component';
import { PublicationComponent } from './pages/publication/publication.component';
import { FamilleComponent } from './pages/famille/famille.component';
import {SondageFormComponent} from "./pages/sondage-form/sondage-form.component";
import {ConnectedGuard} from "./utils/guard/connected/connected.guard";
import {FamilleItemComponent} from "./pages/famille-item/famille-item.component";
import {DocumentComponent} from "./pages/document/document.component";
// import { SondageFormComponent } from './pages/sondage-form/sondage-form.component';

const routes: Routes = [
  {
    path: '',
    component: LoginComponent
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'projet',
    canActivateChild: [ConnectedGuard],
    canActivate: [ConnectedGuard],
    component: ProjetComponent
  },
  {
    path: 'projetAvancement/:projetId',
    canActivateChild: [ConnectedGuard],
    canActivate: [ConnectedGuard],
    component: ProjetAvancementComponent
  },
  {
    path: 'publication',
    canActivateChild: [ConnectedGuard],
    canActivate: [ConnectedGuard],
    component: PublicationComponent
  },
  {
    path: 'famille',
    canActivateChild: [ConnectedGuard],
    canActivate: [ConnectedGuard],
    component: FamilleComponent
  },
  {
    path: 'familleDetail/:familleId',
    canActivateChild: [ConnectedGuard],
    canActivate: [ConnectedGuard],
    component: FamilleItemComponent
  },
  {
    path: 'sondage/:projetId',
    canActivateChild: [ConnectedGuard],
    canActivate: [ConnectedGuard],
    component: SondageFormComponent
  },
  {
    path: 'document',
    canActivateChild: [ConnectedGuard],
    canActivate: [ConnectedGuard],
    component: DocumentComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRouting { }
