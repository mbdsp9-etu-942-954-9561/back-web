import {Component, OnInit} from '@angular/core';
import {NavigationEnd, Router} from '@angular/router';
import {ProjetService} from "../../utils/service/projet/projet.service";
import {IProjet} from "../../utils/interface/IProjet";

@Component({
  selector: 'app-projet',
  templateUrl: './projet.component.html',
  styleUrls: ['./projet.component.scss']
})
export class ProjetComponent implements OnInit{

  currentRoute: string = "";
  formDataList: any[] = [];
  error = "";

  link = "/projetAvancement/";

  constructor(
    private router: Router,
    private projetService: ProjetService
  ) {
    router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        this.currentRoute = event.url;
      }
    });
  }

  ngOnInit(): void {
    this.projetService.getList().subscribe({
      next: (response) => {
        this.formDataList = response;
      },
      error: (error) => this.error = error.message
    })
  }

  addProjet(projet: IProjet ){
    this.formDataList.push(projet);
  }

}
