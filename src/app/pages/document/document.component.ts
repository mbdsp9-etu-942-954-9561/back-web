import {Component, OnInit} from '@angular/core';
import {IDemandeDocument} from "../../utils/interface/IDemandeDocument";
import {DemandeDocumentService} from "../../utils/service/demande-document/demande-document.service";

@Component({
  selector: 'app-document',
  templateUrl: './document.component.html',
  styleUrls: ['./document.component.scss']
})
export class DocumentComponent implements OnInit{
  error = "";

  demandeDocuments: IDemandeDocument[] = [];

  constructor(private demandeDocumentService: DemandeDocumentService) {
  }

  ngOnInit(): void {
    this.demandeDocumentService.getList().subscribe({
      next: (response) => {
        this.demandeDocuments = response;
      },
      error: (error) => this.error = error.message
    })
  }

}
