import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FamilleComponent} from "./famille/famille.component";
import {LoginComponent} from "./login/login.component";
import {ProjetComponent} from "./projet/projet.component";
import {ProjetAvancementComponent} from "./projet-avancement/projet-avancement.component";
import {PublicationComponent} from "./publication/publication.component";
import {SondageFormComponent} from "./sondage-form/sondage-form.component";
import {ComponentsModule} from "../components/components.module";
import {FormsModule} from "@angular/forms";
import {RouterLink} from "@angular/router";
import { FamilleItemComponent } from './famille-item/famille-item.component';
import { DocumentComponent } from './document/document.component';



@NgModule({
  declarations: [
    FamilleComponent,
    LoginComponent,
    ProjetComponent,
    ProjetAvancementComponent,
    PublicationComponent,
    SondageFormComponent,
    FamilleItemComponent,
    DocumentComponent
  ],
  imports: [
    CommonModule,
    ComponentsModule,
    FormsModule,
    RouterLink
  ]
})
export class PagesModule { }
