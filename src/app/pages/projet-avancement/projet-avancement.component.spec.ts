import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProjetAvancementComponent } from './projet-avancement.component';

describe('ProjetAvancementComponent', () => {
  let component: ProjetAvancementComponent;
  let fixture: ComponentFixture<ProjetAvancementComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ProjetAvancementComponent]
    });
    fixture = TestBed.createComponent(ProjetAvancementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
