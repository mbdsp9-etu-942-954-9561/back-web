import {IProjetCategory} from "./IAnnexe";

export interface IProjet {
  id?: number;
  title: string;
  _description: string;
  budget: number;
  projetCategory: IProjetCategory;
  startDate?: Date;
  dueDate?: Date;
}

export interface IProjetPredition {
  projet: IProjet;
  prediction: number
  totalVote: number
  forVote: number
}
