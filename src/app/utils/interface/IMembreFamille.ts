import {IFamille} from "./IFamille";
import {ICitoyen} from "./ICitoyen";

export interface IMembreFamille {

  id?: number;
  family: IFamille;
  citizen: ICitoyen;
  lastupdate?: Date;

}
