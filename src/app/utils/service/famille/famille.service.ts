import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {IFamille} from "../../interface/IFamille";
import {IMembreFamille} from "../../interface/IMembreFamille";

@Injectable({
  providedIn: 'root'
})
export class FamilleService {
  baseUrl = "api/famille/";
  baseMF = "api/membrefamille/";

  constructor(private httpClient: HttpClient) {
    
  }

  getList(): Observable<IFamille[]> {
    const url = this.baseUrl + "read";
    return this.httpClient.get<IFamille[]>(url);
  }

  create(famille: IFamille): Observable<IFamille> {
    const url = this.baseUrl + "create";
    return this.httpClient.post<IFamille>(url, famille);
  }

  getById(id: number): Observable<IFamille> {
    const url = this.baseUrl + "readById/" + id;
    return this.httpClient.get<IFamille>(url);
  }

  getMembreFamille(id: number): Observable<IMembreFamille[]> {
    const url = this.baseUrl + "membrefamille/" + id;
    return this.httpClient.get<IMembreFamille[]>(url);
  }

  addMembreFamille(membre: IMembreFamille): Observable<IMembreFamille> {
    const url = this.baseMF + "create";
    return this.httpClient.post<IMembreFamille>(url, membre);
  }


}
