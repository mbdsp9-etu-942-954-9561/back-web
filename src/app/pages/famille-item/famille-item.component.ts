import {Component, OnInit} from '@angular/core';
import {FamilleService} from "../../utils/service/famille/famille.service";
import {IMembreFamille} from "../../utils/interface/IMembreFamille";
import {ActivatedRoute} from "@angular/router";
import {IFamille} from "../../utils/interface/IFamille";
import {ICitoyen} from "../../utils/interface/ICitoyen";

@Component({
  selector: 'app-famille-item',
  templateUrl: './famille-item.component.html',
  styleUrls: ['./famille-item.component.scss']
})
export class FamilleItemComponent  implements OnInit{
  membreFamilles: IMembreFamille[] = [];
  family = {} as IFamille
  error="";

  constructor(
    private familleService: FamilleService,
    private route: ActivatedRoute
  ) { }

  ngOnInit(): void {
    const familleId = this.route.snapshot.params['familleId']!;
    this.familleService.getMembreFamille(familleId).subscribe({
      next: (response) => {
        this.membreFamilles = response;
      },
      error: (error) => this.error = error.message
    })

    this.familleService.getById(familleId).subscribe({
      next: (response) => {
        this.family = response;
      },
      error: (error) => this.error = error.message
    })
  }

  addMembreFamille(membreFamille: IMembreFamille){
    this.membreFamilles.push(membreFamille);
  }
}
