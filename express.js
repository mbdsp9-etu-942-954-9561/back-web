const express = require('express');
const path = require('path');
const app = express();
const { createProxyMiddleware } = require('http-proxy-middleware');
// Serve static files from the 'dist' directory
app.use(express.static(path.join(__dirname, 'dist/gouv')));

const proxyMiddleware = createProxyMiddleware('/api', {
  target: 'http://85.31.234.130/', // Replace with the target URL you want to proxy to
  changeOrigin: true, // Set this to true for proper host header
  // Additional options if needed
});

app.use('/api', proxyMiddleware);

// Define a catch-all route to serve 'index.html'
app.get('*', (req, res) => {
  res.sendFile(path.join(__dirname, 'dist/gouv/index.html'));
});

// Start the Express server
const port = process.env.PORT || 3000;
app.listen(port, () => {
  console.log(`Server is running on port ${port}`);
});
