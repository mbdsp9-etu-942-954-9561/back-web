import {Component, EventEmitter, Output} from '@angular/core';
import { IPublication } from 'src/app/utils/interface/IPublication';
import {PublicationService} from "../../utils/service/publication/publication.service";

@Component({
  selector: 'app-publication-form',
  templateUrl: './publication-form.component.html',
  styleUrls: ['./publication-form.component.scss']
})
export class PublicationFormComponent {
  publicationForm = {
    label: '',
    publicationDate: new Date(),
    author: '',
  } as IPublication;

  error = "";

  @Output() newPublication = new EventEmitter<IPublication>();

  constructor(
    private publicationService: PublicationService
  ) {}

  onSubmit(): void {
    this.publicationService.create(this.publicationForm).subscribe({
      next: (response) => {
        this.newPublication.emit(response);
        this.publicationForm = {
          label: '',
          publicationDate: new Date(),
          author: '',
        } as IPublication;
      },
      error: (error) => this.error = error.message
    })
  }
}
