import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InvestissementAddComponent } from './investissement-add.component';

describe('InvestissementAddComponent', () => {
  let component: InvestissementAddComponent;
  let fixture: ComponentFixture<InvestissementAddComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [InvestissementAddComponent]
    });
    fixture = TestBed.createComponent(InvestissementAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
