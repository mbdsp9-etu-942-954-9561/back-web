export interface IPublication {
    id: number;
    label: string;
    publicationDate?: Date;
    author?: string;
  }
