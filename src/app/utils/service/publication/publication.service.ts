import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {IPublication} from "../../interface/IPublication";
import {ICommentaire} from "../../interface/ICommentaire";

@Injectable({
  providedIn: 'root'
})
export class PublicationService {

  baseUrl = "/api/publication/";

  constructor(private httpClient: HttpClient) {
  }

  getList(): Observable<IPublication[]> {
    const url = this.baseUrl + "read";
    return this.httpClient.get<IPublication[]>(url);
  }

  getComment(publicatonId:any): Observable<ICommentaire[]> {
    const url = this.baseUrl + "commentaire/" + publicatonId;
    return this.httpClient.get<ICommentaire[]>(url);
  }

  createComment(commentaire: ICommentaire, publicationId: number): Observable<ICommentaire> {
    const url = this.baseUrl + "commenter/" + publicationId;
    return this.httpClient.post<ICommentaire>(url, commentaire);
  }

  create(publication: IPublication): Observable<IPublication> {
    const url = this.baseUrl + "create";
    return this.httpClient.post<IPublication>(url, publication);
  }
}
