import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FamilleItemComponent } from './famille-item.component';

describe('FamilleItemComponent', () => {
  let component: FamilleItemComponent;
  let fixture: ComponentFixture<FamilleItemComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [FamilleItemComponent]
    });
    fixture = TestBed.createComponent(FamilleItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
