import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SondageFormComponent } from './sondage-form.component';

describe('SondageFormComponent', () => {
  let component: SondageFormComponent;
  let fixture: ComponentFixture<SondageFormComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [SondageFormComponent]
    });
    fixture = TestBed.createComponent(SondageFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
