import {Component, EventEmitter, Input, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {IFamille} from 'src/app/utils/interface/IFamille';
import {FamilleService} from "../../utils/service/famille/famille.service";
import {IProjet} from "../../utils/interface/IProjet";
import {ICitoyen} from "../../utils/interface/ICitoyen";

@Component({
  selector: 'app-famille-add',
  templateUrl: './famille-add.component.html',
  styleUrls: ['./famille-add.component.scss']
})
export class FamilleAddComponent {

  familleForm = {} as IFamille;
  error = "";
  @Output() newFamille = new EventEmitter<IFamille>();

  constructor(
    private familleService: FamilleService
  ) {
    this.familleForm.familylead = {} as ICitoyen;
  }

  onSubmit() {
    this.familleService.create(this.familleForm).subscribe({
      next: (response) => {
        this.newFamille.emit(response);
        this.familleForm = {} as IFamille; // Reset the form after submission.
        this.familleForm.familylead = {} as ICitoyen;
      },
      error: (error) => this.error = error.message
    })

  }
}
